# Teamfight Tactics

* 🖥️ Shares the same base client as the MOBA, therefore installation instructions are the same. See [▶️ How to Install League of Legends](../install/index.md).
* 📱 Available on [Android](https://play.google.com/store/apps/details?id=com.riotgames.league.teamfighttactics) and [iOS](https://apps.apple.com/app/id1480616748).
* Elsewhere on reddit at r/teamfighttactics.