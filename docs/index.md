!!! Warning "Important Updates Regarding Reddit"
    :material-reddit: r/leagueoflinux is currently closed in protest of the upcoming reddit API changes. For more information, and how you can protest, see the [stickied r/Save3rdPartyApps thread](https://old.reddit.com/r/Save3rdPartyApps/comments/14goyu2/what_to_do_when_reddit_bans_blackouts_hit_em_in/).

    To view old r/leagueoflinux threads, use the [WayBack Machine](https://web.archive.org/). Full subreddit archival is currently underway.
  
    [📢 League of Linux is Now Active on Kbin at kbin.social/m/leagueoflinux - Come Join!](https://kbin.social/m/leagueoflinux/t/37235/League-of-Linux-is-Now-Active-on-Kbin)

    An announcement regarding Discord/Revolt is coming soon.

# 📜 Welcome to ![text_logo](assets/text_logo.png)!

_^✨^ ^The^ ^shiny^ ^new^ ^home^ ^for^ ^the^ ^r/leagueoflinux^ ^wiki^ ^✨^_

Greetings Summoner ^(are^ ^we^ ^still^ ^allowed^ ^to^ ^say^ ^that?)^! If you’re here looking to play League of Legends, or any of Riot Games' other titles, on your favourite Linux distribution, you've come to the right place! [leagueoflinux.org](https://leagueoflinux.org) is the one-stop shop for anything and everything Riot Games on Linux. From installation and optimisation of the MOBA, to troubleshooting, FAQs, Riots' other games, and more; this site is a comprehensive must-read before your first open source descent onto Summers Rift!

[:simple-riotgames: :simple-linux: Play](install/index.md){ .md-button .md-button--primary }

!!! Success "Currently playable"
    League of Legends is currently playable on Linux systems. See [the status page](status.md) for the latest information.

### 🗺️ Navigation

🖥️ Pages on desktop can be traversed using the menu on the lefthand side, and individual page chapters can be traversed using the menu on the righthand side.

📱 Pages and chapters on mobile can be traversed using the nested hamburger menu on the lefthand side.

### ❤️ A Thank You

A huge thank you and expression of gratitude to all the volunteers who devote time to helping resolve issues, developing tools to make League of Linux a better experience, and everyone who posts and comments. Without the community effort this site would be pretty empty!

This site began its life as the [first](https://web.archive.org/web/20230611125429/https://old.reddit.com/r/leagueoflinux/comments/jmo1m6/megathread_install_methods_anticheat_problemsbugs/) and [second megathreads](https://web.archive.org/web/20220624201757/https://old.reddit.com/r/leagueoflinux/comments/mv1pzp/start_here_league_of_linux_megathread_everything/) on the r/leagueoflinux subreddit. Since then it has undergone many revisions, including the [relaunch as a built-in reddit wiki](https://web.archive.org/web/20230103055738/https://old.reddit.com/r/leagueoflinux/comments/p8uii8/subreddit_update_21082021_new_updated_wiki_post/), before making its way to where we are now at [leagueoflinux.org](https://leagueoflinux.org)! From humble beginnings, it's been a pleasure to serve this community. Thank you all!

---

This site is regularly maintained and is completely [open source](https://gitlab.com/leagueoflinux/leagueoflinux.gitlab.io/)! Contributions are welcome. To suggest changes, please [open an issue](https://gitlab.com/leagueoflinux/leagueoflinux.gitlab.io/-/issues). For further questions or feedback, please reach out to u/TheAcenomad on reddit, or @acenomad@kbin.social.