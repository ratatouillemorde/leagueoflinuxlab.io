# Installing League of Legends via Lutris

## What is Lutris?

From [their homepage](https://lutris.net/):

> Lutris is a video game preservation platform aiming to keep your video game collection up and running for the years to come.
Over the years, video games have gone through many different hardware and software platforms. By offering the best software available to run your games, Lutris makes it easy to run all your games, old and new. 

## How to Install

Commands for Ubuntu and Arch-based distributions can be found below each step. Commands for other distributions and more detailed information can be found in the external links provided. Always ensure you inspect the commands and code you run on your machine, never blindly copy and paste without reading first!

If you are using a distribution that is based on Arch or Ubuntu, such as Pop!_OS (Ubuntu) or EndeavourOS (Arch), then the below commands are also applicable to your system.

1. Install the [Lutris client](https://lutris.net/downloads) for your distribution
    * ??? ubuntu "Ubuntu"
        Ddownload the latest `.deb` from the [Lutris Github page](https://github.com/lutris/lutris/releases) and `sudo apt install` it.
        * `cd Downloads&&sudo apt install ./lutris_*_all.deb`
    * ??? arch "Arch"
        You can get Lutris from the [Arch Extra Repository](https://www.archlinux.org/packages/extra/any/lutris/). Updates on Manjaro are delayed.
        * `sudo pacman -S lutris`
2. Follow the steps in the Lutris wiki page on [Installing Drivers](https://github.com/lutris/docs/blob/master/InstallingDrivers.md)
    * ??? ubuntu "Ubuntu"
        * AMD and Intel GPUs: `sudo add-apt-repository ppa:kisak/kisak-mesa && sudo dpkg --add-architecture i386 && sudo apt update && sudo apt upgrade && sudo apt install libgl1-mesa-dri:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386`
        * Nvidia GPUs: `sudo add-apt-repository ppa:graphics-drivers/ppa && sudo dpkg --add-architecture i386 && sudo apt update && sudo apt install -y nvidia-driver-515 libvulkan1 libvulkan1:i386`
        * ℹ️ Nvidia users please ensure your graphics card is [supported by the 515 driver before installing](https://www.nvidia.com/Download/driverResults.aspx/186156/en-us). These commands are taken directly from the [Lutris documentation](https://github.com/lutris/docs/blob/master/InstallingDrivers.md), they note "Sometimes we forget to update the guide to reference the latest version of the Nvidia driver. You can check the [latest version of the Nvidia driver for your gpu here](https://www.nvidia.com/Download/index.aspx) and then replace `515` in `nvidia-driver-515` with the first part of the version number (the one before the dot, **515**.48.07) that is actually latest."
    * ??? arch "Arch"
        1. Enable the multilib (32-bit) repository by uncommenting the `[multilib]` section in `/etc/pacman.conf`
        2. Update all packages with `sudo pacman -Syu`
        * AMD GPUs: `sudo pacman -S --needed lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader`
        * Intel GPUs: `sudo pacman -S --needed lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader`
        * Nvidia GPUs: `sudo pacman -S --needed nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader`
3. Follow the steps in the Lutris wiki page on [Wine Dependencies](https://github.com/lutris/docs/blob/master/WineDependencies.md) 
    * ??? ubuntu "Ubuntu"
        `sudo dpkg --add-architecture i386 && sudo apt update && sudo apt install -y wine64 wine32 libasound2-plugins:i386 libsdl2-2.0-0:i386 libdbus-1-3:i386 libsqlite3-0:i386`
    * ??? arch "Arch"
        `sudo pacman -S --needed wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses ocl-icd lib32-ocl-icd libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader`
4. Click here to open the installer [:simple-riotgames: :simple-linux: Install](lutris:league-of-legends-standard){ .md-button .md-button--primary }
    * Or manually open the main [League of Legends Lutris page](https://lutris.net/games/league-of-legends/) and click `Install` on the launcher titled `Standard version` for your region
5. Follow the instructions in the Lutris popups, please note:
    * There are two downloads one after the other: the initial client download and the Riot client download. Wait for **both** downloads to finish.
    * Do **not** enter your login details and login or click play. Instead, just quit the launcher when the two downloads have finished.
6. Follow the Lutris popups instructions to finish the install.


!!! info "Optimal performance settings"
    You may need to adjust some settings for optimal performance once installed. Read more in [📈 How to Optimise League of Legends](../optimise/index.md).

!!! failure "Something not working?"
    Read more on common problems, how to solve them, and other troubleshooting tips in [🛠️ Troubleshooting and Technical Support](../troubleshooting/index.md).