# Common Problems and How to Solve Them

## Important Information

This page contains many of the issues that players have faced playing League of Legends on Linux, and step-by-step instructions on how to resolve them. Consult this page in detail before making a support request post. 

As this page is rather comprehensive, problems are broadly broken down into three categories:

* [Game and Graphical Issues](solutions.md#wiki_game_and_graphical_issues): problems that occur in game, or in the process of launching a game
* [Client Issues](solutions.md#wiki_client_issues): problems that occur in the League and Riot clients, and the general out-of-game experience
* [System and Dependency Issues](solutions.md#wiki_system_or_dependency_issues): problems that occur during pre-installation, or that otherwise block launching of the clients

## Game and Graphical Issues


### ℹ️ Vulkan compatibility

Make sure your GPU is [Vulkan-capable](https://en.wikipedia.org/wiki/Vulkan_\(API\)#Hardware). Most modern graphics cards are, however if your GPU is not Vulkan-capable then you must disable DXVK in the Lutris configuration for League:

1. Right click League of Legends in Lutris
2. Select `Configure`
2. Disable `Enable DXVK/VKD3D` then `Save`

Users of older DXVK-unsupported AMD cards may want to read [this comment](https://web.archive.org/web/20230317042857/https://old.reddit.com/r/leagueoflinux/comments/p4eq7n/client_not_startingcrashing_on_startup/h8yj68k/) by community member /u/danalucard.

### ✅ Critical error and anticheat pop ups

If you encounter `... your system is not configured to work with League's anticheat.` pop ups attempting to launch the client, or when the game crashes after champion select with `A critical error has occurred...`, try one of the solutions below:

* Solution 1: Run `sudo sh -c 'sysctl -w abi.vsyscall32=0'` then `reboot`
* Solution 2: Run `sudo sysctl -w abi.vsyscall32=0` before **EVERY** new session you play League
* Solution 3: Manually edit your `/etc/sysctl.conf` and append with the line `abi.vsyscall32=0` then `reboot`
* Solution 4: Install and use [wine-lol](../optimise/index.md#wiki_wine-lol_pkgbuild)

### ✅ 'Sticky' mouse in ping menu, cannot move camera or wrong mouse clicks

If you experience a 'sticky' mouse when interacting with the ping menu or moving your champion in game, cannot move the camera or experience wrong mouse clicks, try the following solutions:

* Solution 1: switch your game to `Windowed borderless` mode in the game settings
* Solution 2: Alt+Tab to a different window, then return back to League
* Solution 3: press your meta/Windows key, then return back to League
* Solution 4: swap workspaces (hotkeys differ with each DE), then return back to League
* Solution 5: enable virtual desktop mode (see the issue directly above this one for instructions)

### ✅ Stuttering or low FPS when moving camera or mouse

* For compatible mice install and configure [Piper](https://github.com/libratbag/piper/) to use a polling rate lower than 1000Hz
* For all other mice you can [manually adjust your polling rate](https://wiki.archlinux.org/title/Mouse_polling_rate#Set_polling_interval) lower than 1000Hz

### ❌ Mouse sensitivity doesn't change in game

* Currently unresolved

* Workaround: install and configure [Piper](https://github.com/libratbag/piper/) with a compatible mouse or otherwise altering the mouse speed via your distro settings

### ✅ Champion or screen lags or glitches when moving camera

 * Solution: disable `Movement prediction` in the in-game settings


## Client Issues

### ✅ Client takes a long time to load after attempting to launch

* Solution: use a newer version of Wine
* This used to be an intended artificial wait time introduced to fix a [websocket bug](https://web.archive.org/web/20230000000000*/https://old.reddit.com/r/leagueoflinux/comments/j0o2qo/new_lutris_installer_with_launch_helper/). This was eventually fixed in later custom Wine builds.

### ❌ Chat and friends list take a long time to load in the client

 * Currently unresolved. The chat and friends list is slow for everyone on Linux, it is believed to be due to the League Client not playing well with websocket implementation in Wine. This does not affect in-game chat, but does also impact pre and post-game chat lobbies.

### ✅ Cannot change game or client resolution or it is stuck at the lowest resolution

Usually the lowest resolution this bug triggers is 1024x576.

* Solution: enable virtual desktop mode in Lutris and borderless window mode in League:
    1. Right Click League of Legends
    2. Select `Configure`
    3. Select `Runner options`
    4. Enable `Windowed (virtual desktop)`
    5. Change `Virtual desktop resolution` to the size of your monitor
    6. Launch the game and in graphics settings change `Window mode` to `Borderless`

### ✅ Riot client error `Could Not Update`

* Solution: use the [official installer](https://signup.na.leagueoflegends.com/en/signup/redownload) to repatch your game by running it within the Wine prefix of your installation. To do this on Lutris:
    1. Select League of Legends in Lutris
    2. Select the 🔺triangle next to Wine symbol
    3. Select `Run EXE inside Wine prefix`
    4. Choose the previously downloaded [official installer](https://signup.na.leagueoflegends.com/en/signup/redownload)
    5. In the Riot Client popup that appears, click `Install`

## System or Dependency Issues

### ✅ Crashing or loading issues using custom or upstream versions of Wine

*  Solution: change to the default version of Wine included with Lutris or `wine-lol`. Custom or upstream versions of Wine are incompatible with League of Legends at this time. For more information see [Why Can't We Play on Upstream Wine?](../faq/index.md#wiki_why_can.27t_we_play_on_upstream_wine.3F)

### ✅ Error pop up for "dialog"

If you encounter the pop up `This installer requires dialog on your system`, you need to install the `dialog` package.

* Solution: install the package `dialog` via your package manager, for example `sudo apt install -y dialog` on Debian-based systems

### ✅ "Wine System Tray" window on Pantheon and GNOME-based desktops

* GNOME solution: install `libappindicator` and the companion AppIndicator GNOME extension
    1. Install `libappindicator` via your distro package manager
    2. Install the GNOME Shell Integration extension for either [Firefox](https://addons.mozilla.org/en-US/firefox/addon/gnome-shell-integration/) or [Chrome](https://chrome.google.com/webstore/detail/gnome-shell-integration/)
    3. Install [AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/)
* Pantheon solution: install a custom `wingpanel-indicator` and locally build `wingpanel-indicator-na-tray`
    1. Add the repo `sudo add-apt-repository ppa:bluesabre/elementary-odin`
    2. Install the custom panel indicator `sudo apt install com.github.lafydev.wingpanel-indicator-ayatana -y`
    3. Download the panel indicator `wget https://github.com/msmaldi/wingpanel-indicator-na-tray/archive/refs/heads/master.zip`
    4. Extract and edit `meson.build`: change the dependency at line 36 from `ibwingpanel_dep = dependency('wingpanel-2.0')` to `libwingpanel_dep = dependency('wingpanel')`
    5. Compile and install the package `meson builddir --prefix=/usr ninja -C builddir sudo ninja -C builddir install`

### ✅ Gamemode errors

If you see either one of  `ERROR: ld.so: object '/usr/$LIB/libgamemodeauto.so.0' from LD_PRELOAD cannot be preloaded (cannot open shared object file): ignored.` or `ERROR: ld.so: object ‘libgamemodeauto.so.0’ from LD_PRELOAD cannot be preloaded (wrong ELF class: ELFCLASS64): ignored.` in your logs.

* This is a known error and does not have any effect on the performance or ability to play games. It can safely be ignored, and is not considered the cause of inability to play League.

### ✅ Incorrect graphical drivers used with an Nvidia GPU in hybrid setups

* Solution: enable Nvidia Prime Render Offload in Lutris:
    1. Select League of Legends in Lutris
    2. Select `Configure`
    3. Select `System options`
    4. Tick to enable `Enable NVIDIA Prime Render Offload`
    5. Tick to enable `Use discrete graphics` then `Save`

### ❌ `GLXBadConfig` error

* 🚧 No concrete known causes or resolutions at this time. Please contact /u/TheAcenomad if you have experience with this issue and potential solutions

### ℹ️ `Code 13568` errors

If your logs end in any one of the following: : `Initial process has exited (return code: 13568)`, `Exit with return code 13568` or `Command exited with code 13568`.

* This is a standard Wine exit code when Windows processes die unexpectedly. It is usually not entirely helpful in determining the cause of the crash and it is certainly not enough information to provide by itself when submitting a support request which requires [verbose logs](logs.md).

### ✅ Tiny or invisible client windows on Hyperland

Solved, but to be written. For the time being, read this thread: hhttps://web.archive.org/web/20230514210729/https://old.reddit.com/r/leagueoflinux/comments/13hmsa7/riot_client_opening_in_extremely_tiny_window/

## Additional Resources

More reading on...

* DXVK or Vulkan: [How to: DXVK](https://github.com/lutris/docs/blob/master/HowToDXVK.md)

* ESync: [How to: ESync](https://github.com/lutris/docs/blob/master/HowToEsync.md)
